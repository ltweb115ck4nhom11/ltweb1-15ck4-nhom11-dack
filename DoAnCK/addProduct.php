<?php 
session_start();
require_once './lib/db.php';
if ($_SESSION["admin"] != 1) {
		header("Location: index.php");
	}
$show_alert = 0;
if (isset($_POST["btnSave"])) {
	$name = $_POST["txtProName"];
	$des = $_POST["txtProDes"];
	$fulldes = $_POST["txtProFullDes"];
	$nation = $_POST["txtProNation"];
	$price = $_POST["txtProPrice"];
	$quantity = $_POST["txtProQuantity"];
	$catid = $_POST["selCatID"];
	$catLoai = $_POST["selLoai"];

	$sql = "INSERT into products(ProName, ProDes, ProFullDes, Nation, ProPrice, Quantity, CatID, View, Sold, NgayNhap, MaLoai)
	values('$name', '$des', '$fulldes', '$nation', $price, $quantity, $catid, 0, 0, now(), $catLoai)";

	$id = write($sql);
	$show_alert = 1;

	$f = $_FILES["fuMain"];
	if ($f["error"] > 0) {

	} else {
		mkdir("./assets/img/Ruou/$id/");

		$tmp_name = $f["tmp_name"];
		$name = $f["name"];
		$destination = "./assets/img/Ruou/$id/show.png";
		move_uploaded_file($tmp_name, $destination);
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Thêm sản phẩm</title>
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css">
</head>
<body>
	<br/>
	<br/>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8 col-lg-offset-2">
				<?php if ($show_alert == 1) : ?>
					<div class="alert alert-success" role="alert">
						<strong>Thêm sản phẩm thành công!</strong> Sản phẩm đã được thêm.
					</div>
				<?php endif; ?>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Thêm Sản Phẩm</h3>
					</div>
					<!-- <div class="panel-body"> -->
						<form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
							</div>
							<div class="form-group">
								<label for="txtProName" class="col-sm-2 control-label">Tên Sản phẩm</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="txtProName" name="txtProName" placeholder="Rượu Gạo">
								</div>
							</div>
							<div class="form-group">
								<label for="txtProName" class="col-sm-2 control-label">Mô Tả Ngắn</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="txtProDes" name="txtProDes" placeholder="Rượu ngon bá cháy">
								</div>
							</div>
							<div class="form-group">
								<label for="txtFullDes" class="col-sm-2 control-label">Mô Tả Chi Tiết</label>
								<div class="col-sm-10">
									<textarea rows="6" id="txtProFullDes" name="txtProFullDes" class="form-control"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="txtProName" class="col-sm-2 control-label">Quốc Gia</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="txtProNation" name="txtProNation" placeholder="Lào">
								</div>
							</div>
							<div class="form-group">
								<label for="txtProName" class="col-sm-2 control-label">Giá</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="txtProPrice" name="txtProPrice" placeholder="690000">
								</div>
							</div>
							<div class="form-group">
								<label for="txtTinyDes" class="col-sm-2 control-label">Số Lượng</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="txtProQuantity" name="txtProQuantity" placeholder="69">
								</div>
							</div>
							<div class="form-group">
								<label for="selCatID" class="col-sm-2 control-label">Nhà Sản Xuất</label>
								<div class="col-sm-10">
									<select id="selCatID" name="selCatID" class="form-control">
										<?php 
											$sql = "select * from categories";
											$rs = load($sql);
											while ($row = $rs->fetch_assoc()) :
										?>
											<option value="<?= $row["CatID"] ?>"><?= $row["CatName"] ?></option>
										<?php endwhile; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="selLoai" class="col-sm-2 control-label">Loại</label>
								<div class="col-sm-10">
									<select id="selLoai" name="selLoai" class="form-control">
										<?php 
											$sql = "select * from loairuou";
											$rs = load($sql);
											while ($row = $rs->fetch_assoc()) :
										?>
											<option value="<?= $row["MaLoai"] ?>"><?= $row["TenLoai"] ?></option>
										<?php endwhile; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="fuMain" class="col-sm-2 control-label">Ảnh Sản Phẩm</label>
								<div class="col-sm-10">
									<input type="file" class="form-control" id="fuMain" name="fuMain">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10" style="margin-top: 20px;">
									<button name="btnSave" type="submit" class="btn btn-success">
										<span class="glyphicon glyphicon-plus"></span>
										Thêm Sản Phẩm Mới
									</button>
									<a href="index.php#SanPham">
										<button name="btnUpdate" type="button" class="btn btn-primary" style="margin-left: 30px;">
										<span class="glyphicon glyphicon-home"></span>
											&nbsp;&nbsp;Quay về Trang Chủ
										</button>
									</a>
								</div>
							</div>
						</form>
					<!-- </div> -->
				</div>
			</div>
		</div>
	</div>
	<script src="assets/jquery-3.1.1.min.js"></script>
	<script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	<script src="assets/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
		tinymce.init({
		    selector: '#txtProFullDes',
		    menubar: false,
		    toolbar1: "styleselect | bold italic | link image | alignleft aligncenter alignright | bullist numlist | fontselect | fontsizeselect | forecolor backcolor | format",
		    // toolbar2: "",
		    // plugins: 'link image textcolor',
		    //height: 300,
		    // encoding: "xml",
		});
	</script>
</body>
</html>