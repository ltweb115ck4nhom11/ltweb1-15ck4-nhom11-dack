<?php
session_start();
if ($_SESSION["admin"] != 1) {
		header("Location: index.php");
	}
require_once 'cart.php';
if(isset($_POST["txtProID"])){
	$proId = $_POST["txtProID"];
	$q = $_POST["txtQuantity"];
	add_item($proId, $q);

	if(isset($_SERVER['HTTP_REFERER'])) {
		$previous = $_SERVER['HTTP_REFERER']."#SanPham";
	    header("Location: $previous");
	}
}
if(isset($_POST["CTtxtProID"])){
	$proId = $_POST["CTtxtProID"];
	$q = $_POST["txtQuantity"];
	add_item($proId, $q);
	$back = $_GET["back"];


	if (isset($_SERVER['HTTP_REFERER'])) {
		$url = $_SERVER['HTTP_REFERER'];
		$temp = explode("back", $url);
		$new_url = $temp[0]."back=".$back;
		header("location: $new_url");
	}
}