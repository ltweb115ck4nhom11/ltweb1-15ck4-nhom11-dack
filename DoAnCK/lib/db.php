<?php

define("HOST", "127.0.0.1");
define("DB", "ruoudb");
define("UID", "root");
define("PWD","");

function load($sql){
	$connect = new mysqli(HOST,UID,PWD,DB);
	if($connect->connect_errno){
		die("FAILED");
	}

	$connect->query("set names 'utf8'");
	
	$rs = $connect->query($sql);
	return $rs;
}

function write($sql){
	$connect = new mysqli(HOST,UID,PWD,DB);
	if($connect->connect_errno){
		die("FAILED");
	}

	$connect->query("set names 'utf8'");
	$connect->query($sql);

	return $connect->insert_id;
}