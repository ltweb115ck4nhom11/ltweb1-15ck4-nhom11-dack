<?php
if (!isset($_SESSION['Mycart'])) {
	$_SESSION['Mycart'] = array();
}

function add_item($proId, $q) {
	if (array_key_exists($proId, $_SESSION['Mycart'])) {
		$_SESSION['Mycart'][$proId] += $q;
	} else {
		$_SESSION['Mycart'][$proId] = $q;
	}
}

function delete_item($proId) {
	unset($_SESSION['Mycart'][$proId]);
}

function update_item($proId, $q) {
	$_SESSION['Mycart'][$proId] = $q;
}

function get_total_items() {
	$total = 0;
	foreach ($_SESSION['Mycart'] as $proId => $q) {
		$total += $q;
	}

	return $total;
}