<?php 
	require_once "../lib/db.php";

	$mail = $_SESSION['mail'];
	if($mail !== "admin"){
		header('Location: ../index.php');
	}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="../assets/bootstrap-3.3.7-dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../assets/index.css">
	<link rel="stylesheet" type="text/css" href="../assets/bootstrap-touchspin-master/dist/jquery.bootstrap-touchspin.min.css">
	
	<script src="../assets/jquery-3.1.1.min.js"></script>
	<script src="../assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	<script src="../assets/bootstrap-touchspin-master/dist/jquery.bootstrap-touchspin.min.js"></script>
	<title>Administrator</title>
</head>
<body>
	 <div class="titleadmin navbar-fixed-top">
		MANAGERMENT YOUR SHOP
	</div>
	<div class="container-fluid">
			<div class="row" >
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 navbar-fixed-top gocn">
				<a href="../index.php"><span class="gohome glyphicon glyphicon-home navbar-fixed-top" aria-hidden="true"></span></a>
				<div class="panel panel-primary" id="Choose">
					<div class="panel-heading ">
						Chức năng
					</div>
					<div class="panel-body">
						<a href="updateloai.php">Cập nhật loại rượu</a><br><br>
						<a href="updatecat.php">Cập nhật NSX</a><br><br>
						<a href="../index.php#SanPham">Cập nhật rượu</a><br><br>
						<a href="manageOrders.php">Duyệt đơn hàng</a><br>
					</div>
				</div>
			</div>
			<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 goct">
				<div class="panel panel-primary" id="Content">
					<div class="panel-heading">
						<?= $tit; ?>
					</div>
					<div class="panel-body">								
						<?php require_once $ndt; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>