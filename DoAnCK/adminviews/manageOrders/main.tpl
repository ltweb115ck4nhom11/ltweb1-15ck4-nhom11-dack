<?php
function changeColor($td) { 
 
    $color1 = '#0094EE'; 
    $color2 = '#2BE973';  
      
    switch ($td) { 
        case 'Đã giao': 
            echo $color1; 
            break; 
        case 'Đang giao': 
            echo $color2; 
            break; 
    } 
}
?>


    <form id="f" method="post" action="updateOrd.php">
              <input type="hidden" id="txtCmd" name="txtCmd">
              <input type="hidden" id="txtOrdId" name="txtOrdId">
    </form>
      <table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Tài khoản</th>
        <th scope="col">Ngày đặt</th>
        <th scope="col">Tổng tiền</th>
        <th scope="col">Tình trạng</th>
      </tr>
    </thead>
      <tbody>
                <?php
                $total = 0;

                  $sql = "select * from orders order by NgayDat DESC";
                  $rs = load($sql);
                  $i = 1;
                  while ($row = mysqli_fetch_assoc($rs)):
                    $ordID = $row["OrderID"];
                    $sql = "Select * from orderdetails where OrderID = $ordID";
                    $rs_details = load($sql);

                    $data = "";
                    
                    while ($row_details = mysqli_fetch_assoc($rs_details)):
                      $proID = $row_details["ProID"];
                      $sql = "Select ProName from products where ProID = $proID";
                      $rs_products = load($sql);
                      $row_products = mysqli_fetch_assoc($rs_products);

                      

                      $data = $data."<b>".$row_products["ProName"]."</b>"."&nbsp;&nbsp;&nbsp;&nbsp;".number_format($row_details["Amount"])."\n";
                      endwhile;
                      $data = $data."<b><u>Địa chỉ:</u></b> ".$row["DiaChi"];
                ?>

                  
                  <tr id="popover" data-content="<?php echo nl2br("$data"); ?>" data-html="true" title="Chi tiết hóa đơn" bgcolor="<?php changeColor($row["TinhTrang"]) ?>">

                    <td><?= $i++ ?></td>
                    <td><?= $row["Email"] ?></td>

                    <?php $date = date_create($row["NgayDat"]);?>
                    <td><?= date_format($date, 'd/m/Y') ?></td>
                    <td><?= number_format($row["TongTien"]) ?> đ</td>
                    <td style="font-weight:bold"><?= $row["TinhTrang"] ?></td>
                    <td class="text-right">
                      <a class="btn btn-xs btn-danger ord-deliver" data-id="<?= $row["OrderID"] ?>" href="javascript:;" role="button">
                        <span class="glyphicon glyphicon-remove"></span>
                      </a>
                      <a class="btn btn-xs btn-success ord-delivering" data-id="<?= $row["OrderID"] ?>" href="javascript:;" role="button">
                        <span class="glyphicon glyphicon-plane"></span>
                      </a>
                      <a class="btn btn-xs btn-primary ord-delivered" data-id="<?= $row["OrderID"] ?>" href="javascript:;" role="button">
                        <span class="glyphicon glyphicon-tags"></span>
                      </a>
                    </td>
                  </tr>
                <?php
                endwhile;
                ?>
                </tbody>
  </table>

<script type="text/javascript">

      $("tr[id=popover]").popover({placement:"top",trigger:"hover"});

      $('.ord-deliver').on('click', function() {
      var id = $(this).data('id');
      $('#txtOrdId').val(id);
        $('#txtCmd').val(-1);
        $('#f').submit();
    });
    $('.ord-delivering').on('click', function() {
      var id = $(this).data('id');
      $('#txtOrdId').val(id);
        $('#txtCmd').val(0);
        $('#f').submit();
    });
    $('.ord-delivered').on('click', function() {
      var id = $(this).data('id');
      $('#txtOrdId').val(id);
        $('#txtCmd').val(1);
        $('#f').submit();
    });
  </script>