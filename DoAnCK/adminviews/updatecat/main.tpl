<?php 
	if (isset($_POST["btnOK"])) {
		$id = $_POST["txtID"];
		$name = $_POST["inputfix"];
		$loai = $_POST["txtLoai"];
		if($name ===""){
			var_dump("Chưa nhập dữ liệu input!");
		}
		else 
		{
			if($loai === "U"){
				$sql = "update categories set CatName = '$name' where CatID = $id";
				$rs = write($sql);
			}
			else if($loai === "I")
			{
				$sql = "select max(CatID) as ID from categories";
				$id = 1;
				$rs = load($sql);
				while($row = mysqli_fetch_assoc($rs)){
					$id = $row["ID"];
				}
				$id = $id + 1;
				$sql = "insert into categories values($id,'$name')";
				$rs = write($sql);
			}
			else if($loai === "D")
			{
				$sql = "delete from categories where CatID = $id";
				$rs = write($sql);
			}
			header('Location: updatecat.php');
		}
	}

	$sql = "select * from categories";
	$rs = load($sql);
	?>
	<table class="table table-hover" id="updatecat">
		<thead>
			<tr>
				<th>CatID</th>
				<th>CatName</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
<?php
	while($row = mysqli_fetch_assoc($rs)):
 ?>
	
		
			<tr>
				<th><?=$row['CatID']?></th>
				<th class="qqtext"><?=$row['CatName']?></th>
				<?php if($row['CatID'] != 7): ?>
				<th>
					<a class="btn btn-warning cat-update" href="javascript:;" role="button" data-id="<?=$row['CatID']?>" 
						data-name="<?=$row['CatName']?>">
						<span class="glyphicon glyphicon-pencil"></span></a>
					<a class="btn btn-danger cat-remove" href="javascript:;" role="button" data-id="<?=$row['CatID']?>"
						data-name="<?=$row['CatName']?>">
						<span class="glyphicon glyphicon-remove"></span></a>
				</th>
				<?php endif; ?>
			</tr>
<?php endwhile; ?>
		</tbody>
	</table>


				<form method="POST" role="form" class="navbar-fixed-top" id ="textboxcat" name="textboxcat">
					<div class="form-group">
						<label for="">Tên NSX</label>
						<input type="text" class="form-control" name="inputfix" id="inputfix" placeholder="Rượu đế Việt Nam ^^">
						<button name="btnOK" id="btnOK" type="submit" class="btn btn-primary">OK</button>
					</div>

					<button type="button" class="btn btn-success buttonaddsp"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
					<input type="hidden" name="txtID" id="txtID">
					<input type="hidden" name="txtLoai" id="txtLoai" value="I">
				</form>

			<script type="text/javascript">
				$('.cat-update').on('click',function(){
					var id = $(this).data('id');
					var q = $(this).data('name');
					$('#inputfix').val(q);
					$('#txtID').val(id);
					$('#txtLoai').val('U');
				});

				$('.cat-remove').on('click',function(){
					if (confirm('Remove?')) {
					    $('#txtLoai').val('D');
						var id = $(this).data('id');
						var q = $(this).data('name');
						$('#inputfix').val(q);
						$('#txtID').val(id);
						$('#btnOK').click();
					} else {
					    // Do nothing!
					}
				});

				$('.buttonaddsp').on('click',function(){
					$('#txtLoai').val('I');
				});
			</script>