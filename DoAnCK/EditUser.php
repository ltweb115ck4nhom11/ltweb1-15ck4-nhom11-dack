<?php
session_start();
require_once './lib/db.php';
if ($_SESSION["LogIn"] != 1) {
		header("Location: index.php");
	}

$show_alert = 0;

if (!isset($_GET["mail"])) {
	header('Location: index.php');
}

if (isset($_POST["btnUpdate"])) {
	$u_name= $_POST["UName"];
	$u_pass = $_POST["UPass"];
	$u_pass = md5($u_pass);
	$u_email = $_POST["UEmail"];
	$u_sql = "update users set Name = '$u_name', Pass = '$u_pass' where Email = '$u_email'";
	//$iii = "insert into users values('$u_name', '$u_email', '$u_pass')";
	write($u_sql);
	//write($iii);
	$show_alert = 1;
}


$mail = $_GET["mail"];
$sql = "select * from users where Email = '$mail'";
$rs = load($sql);
while ($row = $rs->fetch_assoc()) {
	$NAME = $row["Name"];
	$PASS = $row["Pass"];
}
?>


<!DOCTYPE html>
<html>
<head>
	<title>Chỉnh sửa danh mục</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/index.css">
	<script src="assets/jquery-3.1.1.min.js"></script>
	<script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body style="text-align: center; margin-left: 30%;">
	<br>
	<br>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				<?php if ($show_alert == 1) : ?>
					<div class="alert alert-success" role="alert">
						<strong>Cập nhật thông tin thành công!</strong> Thông tin của bạn đã được thay đổi.
					</div>
				<?php endif; ?>
				<form method="post" action="" name="frmEdit">
					<div class="form-group">
						<label>Name</label><br>
						<input type="text" class="form-control" id="txtCatID" placeholder="<?= $NAME ?>" name="UName" value="<?= $NAME ?>">
					</div>
					<div class="form-group">
						<label>Email</label><br>
						<input type="text" class="form-control" id="txtCatName" name="UEmail" placeholder="<?= $mail ?>" value="<?= $mail ?>" readonly>
					</div>
					<div class="form-group">
						<label>Password</label><br>
						<input type="password" class="form-control" id="txtCatName" name="UPass" placeholder="<?= $PASS ?>" value="<?= $PASS ?>">
					</div>
					<button type="submit" class="btn btn-success" name="btnUpdate">
						<span class="glyphicon glyphicon-check"></span>
						Cập nhật
					</button>
					<button type="submit" class="btn btn-danger" name="btnDelete">
						<span class="glyphicon glyphicon-remove"></span>
						<a href="index.php">Quay Lại</a>
					</button>
				</form>
			</div>
		</div>
	</div>
	<script src="assets/jquery-3.1.1.min.js"></script>
	<script src="assets/bootstrap-3.3.7/js/bootstrap.min.js"></script>
</body>
</html>