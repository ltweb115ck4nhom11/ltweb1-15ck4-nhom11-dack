<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="MoiNhat">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Mới nhất</h3>
			</div>
			<div class="panel-body">
				<?php $_SESSION['index'] = 1;
 				$_SESSION['sql'] = "select * from products ORDER BY NgayNhap desc limit 10";
 				require "views/productsByCat/main.tpl"; ?>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="BanNN">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Bán chạy nhất</h3>
			</div>
			<div class="panel-body">
				<?php $_SESSION['index'] = 1;
 				$_SESSION['sql'] = "select * from products ORDER BY Sold desc limit 10";
 				require "views/productsByCat/main.tpl"; ?>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="XemNN">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Xem nhiều nhất</h3>
			</div>
			<div class="panel-body">
				<?php $_SESSION['index'] = 1;
 				$_SESSION['sql'] = "select * from products ORDER BY View desc limit 10";
 				require "views/productsByCat/main.tpl"; ?>
			</div>
		</div>
	</div>
</div>