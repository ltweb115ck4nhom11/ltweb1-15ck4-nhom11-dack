<?php


	if (isset($_POST["txtAddress"])) {			
		        $o_Total = $_POST["txtTotal"];
		        $o_UserID = $_SESSION["mail"];
		        $o_OrderDate = strtotime("+7 hours", time());
		        $str_OrderDate = date("Y-m-d H:i:s", $o_OrderDate);
		        $o_Address = $_POST["txtAddress"];
			    $sql = "insert into orders(Email, NgayDat, TongTien, DiaChi) values('$o_UserID', '$str_OrderDate', $o_Total, '$o_Address')";
		        $o_ID = write($sql);



		foreach ($_SESSION["Mycart"] as $proId => $q) {
		        $sql = "select * from products where ProID = $proId";
		        $rs = load($sql);
		        $row = $rs->fetch_assoc();
		        $price = $row["ProPrice"];
		        $amount = $q * $price;
		        $d_sql = "insert into orderdetails(OrderID, ProID, Quantity, Price, Amount) values($o_ID, $proId, $q, $price, $amount)";
		        write($d_sql);

		        $u_sql = "Update products Set Sold = Sold+$q Where ProID = $proId";
		        write($u_sql);

		        $u_quantity = "Update products Set Quantity = Quantity-$q Where ProID = $proId";
		        write($u_quantity);
		    }

		    $_SESSION["Mycart"] = array();
	}
?>


					<div class="panel-body">
						<form id="f" method="post" action="updateCart.php">
							<input type="hidden" id="txtCmd" name="txtCmd">
							<input type="hidden" id="txtDProId" name="txtDProId">
							<input type="hidden" id="txtUQ" name="txtUQ">
						</form>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Sản phẩm</th>
									<th>Giá</th>
									<th class="col-md-2">Số lượng</th>
									<th>Thành tiền</th>
									<th>&nbsp;</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$total = 0;
							foreach ($_SESSION["Mycart"] as $proId => $q) :
								$sql = "select * from products where ProID = $proId";
								$rs = load($sql);
								$row = $rs->fetch_assoc();
								$amount = $q * $row["ProPrice"];
								$total += $amount;
								$sl = $row["Quantity"];
							?>
								<tr>
									<td><?= $row["ProName"] ?></td>
									<td><?= number_format($row["ProPrice"]) ?></td>
									<!-- <td><?= $q ?></td> -->
									<td>
										<input class="quantity-textfield" type="text" name="" id="" value="<?= $q ?>">
									</td>
									<td><?= number_format($amount) ?></td>
									<td class="text-right">
										<a class="btn btn-xs btn-danger cart-remove" data-id="<?= $proId ?>" href="javascript:;" role="button">
											<span class="glyphicon glyphicon-remove"></span>
										</a>
										<a class="btn btn-xs btn-primary cart-update" data-id="<?= $proId ?>" data-max="<?= $sl ?>" href="javascript:;" role="button">
											<span class="glyphicon glyphicon-ok"></span>
										</a>
									</td>
								</tr>
							<?php
							endforeach;
							?>
							</tbody>
							<tfoot>
								<td>
									<a class="btn btn-success" href="index.php#SanPham" role="button">
										<span class="glyphicon glyphicon-backward"></span>
										Tiếp tục mua hàng
									</a>
								</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td><b><?= number_format($total) ?></b></td>
								<td class="text-right">
								
									<button name="btnCheckOut" id="CheckOut" <?php if(empty($_SESSION["Mycart"])){?> disabled = "disabled" <?php }?> class="btn btn-primary">
										<span class="glyphicon glyphicon-usd"></span>
										Thanh toán
									</button>

								</td>
							</tfoot>
						</table>

						<form id="f2" method="POST" action="">
							<input type="hidden" name="txtTotal" value="<?= $total ?>">
							<div class="form-group">
							    <label for="exampleInputEmail1">Địa chỉ giao hàng</label>
							    <input type="text" name="txtAddress" id="txtAddress" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Địa chỉ">
							    <small id="emailHelp" class="form-text text-muted">Nhân viên sẽ giao hàng đến địa chỉ bạn nhập.</small>
						    </div>
						</form>


					</div>

	<script type="text/javascript">
		$('.quantity-textfield').TouchSpin({
	        min: 1,
	        max: 200,
	        verticalbuttons: true,
	    });
	    $('.cart-remove').on('click', function() {
			var id = $(this).data('id');
			$('#txtDProId').val(id);
		    $('#txtCmd').val('D');
		    $('#f').submit();
		});
		$('.cart-update').on('click', function() {
			var sl = $(this).data('max');
			var q = $(this).closest('tr').find('.quantity-textfield').val();
			if(q <= 0)
			{
				q = 1;
			}
			else if(q>sl)
			{
				alert('Số lượng cao nhất là '+sl);
				q = sl;
			}
			$('#txtUQ').val(q);

			var id = $(this).data('id');
			$('#txtDProId').val(id);
		    $('#txtCmd').val('U');

		    $('#f').submit();
		});

		$('#CheckOut').on('click', function(){
			var address = $('#txtAddress').val();
			if(address==="")
			{
				alert("Bạn chưa nhập địa chỉ");
			}
			else
			{
				$('#f2').submit();
			}
		})
	</script>