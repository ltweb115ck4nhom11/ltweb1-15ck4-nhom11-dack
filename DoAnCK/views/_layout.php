<?php
require_once './lib/db.php';
require_once 'cart.php';
if (!isset($_SESSION["LogIn"])) {
	$_SESSION["LogIn"] = 0;
	$_SESSION["mail"] = 0;
	$_SESSION["name"] = 0;
	$_SESSION["admin"] = 0;
}

if ($_SESSION["LogIn"] == 0) {
	if(isset($_COOKIE["auth_user_id"])) {
		$user_id = $_COOKIE["auth_user_id"];
		if($user_id == "admin"){
			$_SESSION["admin"] = 1;
			$_SESSION["mail"] = "admin";
		}
		$sql = "select * from users where Email = '$user_id'";
		$rs = load($sql);
		if ($rs->num_rows > 0) {
			$_SESSION["LogIn"] = 1;
			$row = $rs->fetch_assoc();
			$_SESSION["name"] = $row["Name"];
		}
	}
}

if (isset($_POST["DangNhap"])) {
	$email = $_POST["EMAIL"];
	$pass = $_POST["PASS"];
	$enc_password = md5($pass);
	?>
	<?php
	$sql = "select * from users";
	$rs = load($sql);
	while ($row = $rs->fetch_assoc()) :
		?>
		<?php 
		if ($row["Email"] == $email && $row["Pass"] == $enc_password){
			?>
			<?php
			$_SESSION["LogIn"] = 1;
			$_SESSION["name"] = $row["Name"];
			$_SESSION["mail"] = $email;

			if(isset($_POST["cbRemember"])) {
				$user_id = $_POST["EMAIL"];
				setcookie("auth_user_id", $user_id, time() + 86400);
			}
			else{
				setcookie("auth_user_id","", time() - 3600);
			}
		}
		?>
		<?php
		if ($email == 'admin' && $enc_password == md5('admin123')){
			?>
			<?php
			$_SESSION["admin"] = 1;
		}
		?>
		<?php
	endwhile;
	?>
	<?php
	if ($_SESSION["LogIn"] == 0) {
		echo '<script language="javascript">';
		echo 'alert("Username hoặc Password của bạn không đúng\n Vui lòng kiểm tra lại");';		
		echo '</script>';
	}
}
?>


<?php

require_once './vendor/autoload.php';
use Gregwar\Captcha\CaptchaBuilder;

if (isset($_POST["DangKy"])) {
	$input = $_POST["txtCapCha"];
	$p1 = $_POST["PASSSU"];
	$p2 = $_POST["REPASSSU"];
	if ($input == $_SESSION["captcha"] && $p1 == $p2) {
		$namesg = $_POST["NAMESU"];
		$emailsg = $_POST["EMAILSU"];
		$passsg = $_POST["PASSSU"];
		$enc_passwordsg = md5($passsg);

		$sqlkt = "select * from users where Email = '$emailsg'";
		$rskt = load($sqlkt);
		if ($rskt->num_rows > 0) {
			echo '<script language="javascript">';
			echo 'alert("Email đã tồn tại!")';
			echo '</script>';
		} else {
			$sqlsg = "insert into users(Name, Email, Pass) values('$namesg', '$emailsg', '$enc_passwordsg')";
			write($sqlsg);

			echo '<script language="javascript">';
			echo 'alert("Đăng ký thành công, mời bạn đăng nhập !!!")';
			echo '</script>';
		}
		
	}
	elseif ($p1 != $p2) {
		echo '<script language="javascript">';
		echo 'alert("Mật khẩu nhập không khớp :(")';
		echo '</script>';
	}
	else{
		echo '<script language="javascript">';
		echo 'alert("CapCha của bạn không đúng\n Vui lòng kiểm tra lại")';
		echo '</script>';
	}
}
?>
<?php 
if(isset($_POST['txttimkiem'])){
	$search = $_POST['txttimkiem'];
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Wine Shop</title>

	<link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/index.css">
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-touchspin-master/dist/jquery.bootstrap-touchspin.min.css">

	<script src="assets/jquery-3.1.1.min.js"></script>
	<script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	<script src="assets/bootstrap-touchspin-master/dist/jquery.bootstrap-touchspin.min.js"></script>
</head>
<body>	

	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>                        
				</button>
				<a class="navbar-brand" href="index.php">
					<span class="glyphicon glyphicon-home"></span>
				</a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav navbar-left">
					<li><a href="#SanPham">SẢN PHẨM</a></li>
					<li><a href="#NhaSX">NHÀ SẢN XUẤT</a></li>
					<li><a href="#LoaiSP">LOẠI RƯỢU</a></li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">THÊM
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li><a href="index.php#MoiNhat">Mới nhất</a></li>
							<li><a href="index.php#BanNN">Bán chạy nhất</a></li>
							<li><a href="index.php#XemNN">Xem nhiều nhất</a></li>
						</ul>
					</li>
				</ul>
				<form class="navbar-form navbar-left">
					<select id="typesearch" class="btn btn-default dropdown-toggle" style="margin-top: 5px;">
						<option value="1">Tên SP</option>
						<option value="2">Nhà SX</option>
						<option value="3">Giá</option>
						<option value="4">Xuất xứ</option>
						<option value="5">Loại</option>
					</select>
					
					<div class="input-group">
						<input id="txttimkiem" type="text" class="form-control" placeholder="Tìm kiếm">
						<div class="input-group-btn">

							<button id="btntimkiem" class="btn btn-default" type="submit" style="margin-top: 10px;">

								<i class="glyphicon glyphicon-search"></i>
							</button>
						</div>
					</div>
				</form>
				<ul class="nav navbar-nav navbar-right" style="margin-right: 20px;">
					<?php if ($_SESSION["LogIn"] == 0) : ?>

						<li onclick="document.getElementById('SignUp').style.display='block'; document.getElementById('LogIn').style.display='none'" ><a href="#"><span class="glyphicon glyphicon-user" ></span> ĐĂNG KÝ</a></li>
						<li onclick="document.getElementById('LogIn').style.display='block'; document.getElementById('SignUp').style.display='none'"><a href="#"><span class="glyphicon glyphicon-log-in"></span> ĐĂNG NHẬP</a></li>
					</ul>
				<?php endif; ?>

				<?php if ($_SESSION["LogIn"] == 1) : ?>
					<?php if ($_SESSION["admin"] == 1) { ?>
					<li><a href="adminviews/manageOrders.php"><span class="glyphicon glyphicon-shopping-cart"></span> Duyệt đơn hàng</a></li>
					<?php }else{ ?>
					<li><a href="view_cart.php#SanPham"><span class="glyphicon glyphicon-shopping-cart"></span> GIỎ HÀNG (<?= get_total_items(); ?>)</a></li>
					<?php } ?>
					<ul class="nav navbar-nav navbar-right">
						<div class="dropdown">
							<button class="btn btn-default dropdown" type="button" id="menu1" data-toggle="dropdown" style="background-color: black;"><li style="float: right;"><a href="#"><span class="glyphicon glyphicon-user"></span>&nbsp;<?= $_SESSION["name"] ?></a></li>
								<span class="caret"></span></button>
								<ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
									<?php if ($_SESSION["admin"] == 1) : ?>
										<li role="presentation"><a role="menuitem" tabindex="-1" href="addProduct.php">Thêm sản phẩm</a></li>
										<li role="presentation"><a role="menuitem" tabindex="-1" href="adminviews/manageOrders.php">Duyệt đơn hàng</a></li>
									<?php else: ?>
										<li role="presentation"><a role="menuitem" tabindex="-1" href="EditUser.php?mail=<?=$_SESSION["mail"]?>">Chỉnh sửa thông tin</a></li>
										<li role="presentation"><a role="menuitem" tabindex="-1" href="lichsumuahang.php#SanPham">Lịch sử mua hàng</a></li>
									<?php endif; ?>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="LogOut.php">Đăng Xuất</a></li>
								</ul>
							</div>

						</ul>
					<?php endif; ?>
				</div>
			</div>
		</nav>

		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
				<li data-target="#myCarousel" data-slide-to="3"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="assets/img/wine1.jpg" alt="Wine 1" width="1200" height="800">
					<div class="carousel-caption">
						<h3>Rosso</h3>

					</div>      
				</div>

				<div class="item">
					<img src="assets/img/wine2.jpg" alt="Wine 2" width="1200" height="800">
					<div class="carousel-caption">
						<h3>Chivas</h3>

					</div>      
				</div>

				<div class="item">
					<img src="assets/img/wine3.jpg" alt="Wine 3" width="1200" height="800">
					<div class="carousel-caption">
						<h3>Vodka</h3>

					</div>      
				</div>
				<div class="item">
					<img src="assets/img/wine4.jpg" alt="Wine 4" width="1200" height="800">
					<div class="carousel-caption">
						<h3>Martell</h3>

					</div>      
				</div>
			</div>



			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>



		<div id="SignUp" class="modal col-sm-6 col-md-4">
			<form method="POST" action="" class="modal-content animate" id="g" role="form" onsubmit="return checkEmail();">
				<div class="container">
					<label>Name</label><br>
					<input class="inputGR" type="text" id="NAMESU" name="NAMESU" placeholder="Enter Name" required><br>

					<label>Email</label><br>
					<input class="inputGR" type="text" id="EMAILSU" name="EMAILSU" placeholder="Enter Email" required><br>

					<label>Password</label><br>
					<input class="inputGR" type="password" id="PASSSU" name="PASSSU" placeholder="Enter Password" required><br>

					<label>Repeat Password</label><br>
					<input class="inputGR" type="password" id="REPASSSU" name="REPASSSU" placeholder="Repeat Password" required><br>


					<div class="form-group">
						<label>CapCha</label>&nbsp;&nbsp;&nbsp;
						<?php
						$builder = new CaptchaBuilder;
						$builder->build();
						$_SESSION["captcha"] = $builder->getPhrase();	
						?>
						<img src="<?= $builder->inline() ?>" alt="captcha" />
						<input class="inputCA" type="text" id="txtCapCha" name="txtCapCha" placeholder="Enter CapCha" required>
					</div>

				</div>
				<div class="clearfix">
					<button type="button" onclick="document.getElementById('SignUp').style.display='none'" class="cancelbtn">Cancel</button>
					<button type="submit" id="DangKy" class="signupbtn" name="DangKy">Sign Up</button>
				</div>
			</form>
		</div>

		<div id="LogIn" class="modal col-sm-6 col-md-4">
			<form method="post" action="" name="frmAdd" class="modal-content animate">
				<div class="container">

					<label>Email</label><br>
					<input class="inputGR" type="text" id="Email" name="EMAIL" placeholder="Enter Email" required><br>

					<label>Password</label><br>
					<input class="inputGR" type="password" id="Password" name="PASS" placeholder="Enter Password" required><br>

					<input type="checkbox" checked="checked" name="cbRemember">Remember me
				</div>
				<div class="clearfix">
					<button type="button" onclick="document.getElementById('LogIn').style.display='none'" class="cancelbtn">Cancel</button>
					<button type="submit" class="signupbtn" name="DangNhap">Log In</button>
				</div>
			</form>
		</div>


		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
				<!-- <div class="panel panel-primary" id="SanPham">
					<div class="panel-heading">
						<h3 class="panel-title">Sản phẩm</h3>
					</div>
				</div> -->
				
				<div class="panel panel-primary" id="NhaSX">
					<div class="panel-heading">
						<h3 class="panel-title">Nhà sản xuất</h3>
					</div>
					<div class="panel-body">
						<?php 
						$sql = "select * from categories";
						$rs = load($sql);
						while($row = mysqli_fetch_assoc($rs)):
							if($row["CatName"] != "Other"){
							?>
							<a href="productsByCat.php?id=<?= $row["CatID"]?>#SanPham" class="panel-body-item"><?= $row["CatName"] ?></a>
							<br><br>
							<?php } ?>
						<?php endwhile; ?>
						<a href="productsByCat.php?id=7#SanPham" class="panel-body-item">Other</a>
							<br><br>
						<?php if ($_SESSION["admin"] == 1) { ?>
						<a class="btn btn-primary" href="adminviews/updatecat.php" role="button">
							<span class="glyphicon glyphicon-adjust"></span>
							Chỉnh sửa
						</a>
						<?php } ?>
					</div>
				</div>

				<div class="panel panel-primary" id="LoaiSP">
					<div class="panel-heading">
						<h3 class="panel-title">Loại rượu</h3>
					</div>
					<div class="panel-body">
						<?php 
						$sql = "select * from loairuou";
						$rs = load($sql);
						while($row = mysqli_fetch_assoc($rs)):
							if($row["TenLoai"] != "Other"){
							?>
							<a href="productsByCat.php?maloai=<?= $row["MaLoai"]?>#SanPham" class="panel-body-item"><?= $row["TenLoai"] ?></a>
							<br><br>
							<?php } ?>
						<?php endwhile; ?>
						<a href="productsByCat.php?maloai=5#SanPham" class="panel-body-item">Other</a>
							<br><br>
						<?php if ($_SESSION["admin"] == 1) { ?>
						<a class="btn btn-primary" href="adminviews/updateloai.php" role="button">
							<span class="glyphicon glyphicon-adjust"></span>
							Chỉnh sửa
						</a>
						<?php } ?>
					</div>
				</div>
			</div>

			

			<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
				<div class="panel panel-primary" id="SanPham">
					<div class="panel-heading">
						<h3 class="panel-title"><?=$Title;?></h3>
					</div>
					<div class="panel-body">								
						<?=require_once "$nd";?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<p>Sản phẩm được hợp tác hoàn thiện bởi</p>
		<p>Trần Minh Bảo/Nguyễn Duy Quyết/Phan Nguyễn Ngọc Quỳnh</p>
		<p>Trường Đại học Khoa Học Tự Nhiên, số 227 Nguyễn Văn Cừ phường 4 quận 5</p>
		<p>Copyright © 15CK4</p>
	</div>
	<script type="text/javascript">
		$('#btntimkiem').on('click',function(){
			window.close();
			window.open('TimKiem.php?search='+document.getElementById('txttimkiem').value+'&type='+typesearch.value+'#SanPham');
		});
		function checkEmail() { 
			var email = document.getElementById('EMAILSU'); 
			var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/; 
			if (!filter.test(email.value)) { 
				alert('Hãy nhập địa chỉ Email hợp lệ.\nExample@gmail.com');
				email.focus; 
				return false;
			}
			var pass = $('#PASSSU').val();
			var repass = $('#REPASSSU').val();
			if (pass != repass)
			{
				alert('Mật khẩu xác nhận không đúng');
				return false;
			}
		} 
	</script>
</body>
</html>