<?php
	$sql = "select * from orders where Email = '$mail'"." order by OrderID desc";
	$rd = load($sql);
	$num_rows = $rd->num_rows;
	$page = ceil($num_rows/3);

	$current_page = 1;
	if(isset($_GET['page']))
	{
		$current_page = $_GET['page'];
		if($current_page <= 0){
			$current_page = 1;
		}
		else if($current_page > $page){
			$current_page = $page;
		}
	}
	
	$next_page = $current_page + 1;
	$previous_page = $current_page - 1;
	$c_sql = $sql." Limit ".(($current_page - 1)*3).",3";
	$c_load = load($c_sql);
	while($row = mysqli_fetch_assoc($c_load)):
		$tongtien = $row['TongTien'];
		$ngay = date("d/m/Y",strtotime($row['NgayDat']));
		$eachsql = "select o.OrderID,o.ProID,p.ProName,o.Quantity,p.ProPrice,o.Amount from orderdetails o join products p on o.ProID = p.ProID where o.OrderID = ".$row['OrderID'];
		$eachrs = load($eachsql);
		$diachi = $row['DiaChi'];
		
?>
<div class="container-fluid">
	<table class="table table-striped table-hover table-bordered">
		<thead>
			<tr>
				<th>Mã đơn hàng</th>
				<th style="width: 20%;">Tên SP</th>
				<th></th>
				<th>Số lượng</th>
				<th>Đơn giá</th>
				<th>Thành tiền</th>
			</tr>
		</thead>
		<tbody>
			<th rowspan="<?=$eachrs->num_rows+1?>"><?=$row['OrderID']?></th>
			<?php while($eachrow = mysqli_fetch_assoc($eachrs)): ?>

			<tr>	
				<th><?=$eachrow['ProName']?></th>
				<th><img src="./assets/img/Ruou/<?=$eachrow['ProID']?>/show.png" width="100px" height="100px"></th>
				<th><?=$eachrow['Quantity']?></th>
				<th><?=number_format($eachrow['ProPrice'])?>đ</th>
				<th><?=number_format($eachrow['Amount'])?>đ</th>

			</tr>
			<?php endwhile; ?>
		</tbody>
		<tfoot>
			<tr>
				<th>Ngày đặt</th>
				<th><?=$ngay?></th>
				<th colspan="2">Địa chỉ: <?=$diachi?></th>

				<th>Tổng tiền</th>
				<th style="color:red;"><?=number_format($tongtien);?>đ <?php if($row['TinhTrang'] == "Đã giao"){
					?><span style="color:green;"><?php echo " (Đã nhận)";}
					else if ($row['TinhTrang'] == "Đang giao"){
						?><span style="color:orange;"><?php echo " (Đang giao)";
					}
					else{
						?><span style="color:red;"><?php echo " (Chưa giao)";	
					}?></span></th>
			</tr>
		</tfoot>
	</table>
	<br>
	<br>
</div>
<?php endwhile; ?>
<div style="text-align: center;">
<?php 	if ($previous_page > 0) :?>	
				<a class="btn btn-primary" href="?page=<?=$previous_page?>#SanPham" role="button">
					<span class="glyphicon glyphicon-arrow-left"></span>
						Prev
				</a>
			<?php endif;
		if ($next_page <= $page) :?>
			<a class="btn btn-primary" href="?page=<?=$next_page?>#SanPham" role="button">
				<span class="glyphicon glyphicon-arrow-right"></span>
						Next
			</a>
		<?php endif;?>
</div>