<?php
session_start();
if (isset($_SESSION["LogIn"])) {
	unset($_SESSION["LogIn"]);
	unset($_SESSION["name"]);
	unset($_SESSION["mail"]);
	unset($_SESSION["admin"]);
	unset($_SESSION['Mycart']);
	setcookie("auth_user_id","", time() - 3600);
}

header('Location: index.php');