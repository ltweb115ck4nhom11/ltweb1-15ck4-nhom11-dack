<?php require_once "lib/db.php";
	session_start();
	if (!isset($_SESSION["LogIn"])) {
		$_SESSION["LogIn"] = 0;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Xem chi tiết sản phẩm</title>
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/index.css">
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-touchspin-master/dist/jquery.bootstrap-touchspin.min.css">

	<script src="assets/jquery-3.1.1.min.js"></script>
	<script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	<script src="assets/bootstrap-touchspin-master/dist/jquery.bootstrap-touchspin.min.js"></script>
</head>
<body style="background-color: black;">
	<?php


		$proid = $_GET["proid"];
		$login = $_SESSION["LogIn"];
		$catid = $_GET["catid"];
		$back = $_GET["back"];
		$sql = "select * from products where ProID = $proid";

		$rs = load($sql);
		$sqlcat = "select CatName from categories where CatID = $catid";
		$rscat = load($sqlcat);
		$sqlupxem = "update products set View = View + 1 where ProID = $proid";
		write($sqlupxem);

		
	?>
	<br><br>

	<a class="btn btn-info btn-lg" style="margin-left: 100px;" onclick="window.history.go(<?=$back?>); return false;">
          <span class="glyphicon glyphicon-backward" ></span> Quay lại
        </a>
	<?php
		$row = mysqli_fetch_assoc($rs);
		$rowcat = mysqli_fetch_assoc($rscat);
		$maloai = $row["MaLoai"];
	?>
	<div class="cttop">
	<div>
		<div class="thumbnail" style="margin-left: 300px; float:left;">
			<img src="assets/img/Ruou/<?=$row["ProID"]?>/show.png" width = 300>
		</div>	
		<div style=" font-size:20px;color: green;margin-left: 600px;">
			<form method="post" action="addItemToCart.php?back=<?=$back-1?>" name="f" id="f">
				<ul>
					<li><?=$row["ProName"]?></li>
					<h2><?=number_format($row["ProPrice"]);?> đ &nbsp;&nbsp;</h2>
					<li>Lượt xem: <?=$row["View"]?></li>
					<li>Đã bán: <?=$row["Sold"]?></li>
					<h2>Còn:  <?=$row["Quantity"];?></h2>
					<input class="conlai" type="hidden" value="<?= $row["Quantity"] ?>">
					<input type="hidden" name="CTtxtProID" value="<?= $proid ?>">
					<?php if($row["Quantity"] > 0):?>
					<input name="txtQuantity"  class=" quantity-textfield" type="text" name="" id="txtQuantity" value="1" >		
					<button onclick="Addcart(<?=$_SESSION["LogIn"]?>);" name="btnAddItemToCart" type="button" style="width: 150px;" class="btn btn-success">
						<span class="glyphicon glyphicon-shopping-cart"></span>
						Thêm vào giỏ
					</button>
					 <?php else: ?>
						<button type="button" style="width: 150px;" class="btn btn-danger">
						<span class="glyphicon glyphicon-fire"></span>
						HẾT HÀNG
					</button>
					 <?php endif; ?>
					<li>Xuất xứ: <?=$row["Nation"]?></li>
					<li>Nhà sản xuất: <?=$rowcat["CatName"];?></li>	
				</ul>
			</form>
		</div>
	</div>
	

	<div style=" height: 200px; margin-bottom: 200px;"><h3 style="font-size:16pt;margin-left: 650px; margin-right: 150px; color:yellow;"><?=$row["ProFullDes"];?></h3></div>
	</div>

	<div class="adding" style="margin-top: 200px">
		<?php 
			$sqladd = "select * from products where CatID = '".$catid."' and ProID != $proid order by view desc limit 5";
			$getadd = load($sqladd);

			$array = array();

			if($getadd->num_rows > 0):
				while($rowadd = mysqli_fetch_assoc($getadd)):
					array_push($array,$rowadd["ProID"]);
		 ?>
		 <div style="width: 100%;">
		 		<a href="ChiTiet.php?proid=<?=$rowadd["ProID"]?>&catid=<?=$rowadd["CatID"]?>&back=<?=$back-1?>">
		 			<img src="assets/img/Ruou/<?=$rowadd["ProID"]?>/show.png" width = "100px" style = "float:left; margin-right: 100px;">
		 		</a>
		 </div>
		<?php endwhile; ?>
	<?php endif; ?>
	</div>

	<div class="adding">
		<?php 
			$sqladdd = "select * from products where MaLoai = '".$maloai."' and ProID != $proid order by view desc limit 5";
			$getaddd = load($sqladdd);
			if($getaddd->num_rows > 0):
				while($rowaddd = mysqli_fetch_assoc($getaddd)):
					if(!in_array($rowaddd["ProID"],$array)){
		 ?>
		 <div style="width: 100%;">
		 		<a href="ChiTiet.php?proid=<?=$rowaddd["ProID"]?>&catid=<?=$rowaddd["CatID"]?>&back=<?=$back-1?>">
		 			<img src="assets/img/Ruou/<?=$rowaddd["ProID"]?>/show.png" width = "100px" style = "float:left; margin-right: 100px;">
		 		</a>
		 </div>
		 <?php } ?>
		<?php endwhile; ?>
	<?php endif; ?>
	</div>



	<script type="text/javascript">
		var temp = $('.conlai').val()
		$('.quantity-textfield').TouchSpin({
	        min: 1,
	        max: temp,
	        verticalbuttons: true,
	    });
	</script>
</body>
</html>

<script type="text/javascript">
function Addcart(a){
		var log = a;
		if(log == 1)
		{
			$('#f').submit();
		}
		else{
			confirm('Bạn phải đăng nhập để đặt mua sản phẩm :((');
		}
}
</script>