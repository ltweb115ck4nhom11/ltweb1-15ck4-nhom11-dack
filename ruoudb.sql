/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : ruoudb

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2018-01-09 22:07:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `CatID` int(2) NOT NULL,
  `CatName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`CatID`),
  UNIQUE KEY `PK_CatID` (`CatID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', 'Chivas');
INSERT INTO `categories` VALUES ('2', 'Hennessy');
INSERT INTO `categories` VALUES ('3', 'Martell');
INSERT INTO `categories` VALUES ('4', 'RemyMartin');
INSERT INTO `categories` VALUES ('5', 'Vodka');
INSERT INTO `categories` VALUES ('6', 'Rosso');
INSERT INTO `categories` VALUES ('7', 'Other');

-- ----------------------------
-- Table structure for loairuou
-- ----------------------------
DROP TABLE IF EXISTS `loairuou`;
CREATE TABLE `loairuou` (
  `MaLoai` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `TenLoai` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MaLoai`),
  UNIQUE KEY `PK_MaLoai` (`MaLoai`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of loairuou
-- ----------------------------
INSERT INTO `loairuou` VALUES ('1', 'XO');
INSERT INTO `loairuou` VALUES ('2', 'Very Special');
INSERT INTO `loairuou` VALUES ('3', 'Vang');
INSERT INTO `loairuou` VALUES ('4', 'Thuốc');
INSERT INTO `loairuou` VALUES ('5', 'Other');

-- ----------------------------
-- Table structure for orderdetails
-- ----------------------------
DROP TABLE IF EXISTS `orderdetails`;
CREATE TABLE `orderdetails` (
  `ID` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `OrderID` int(5) NOT NULL,
  `ProID` int(5) unsigned NOT NULL,
  `Quantity` int(5) DEFAULT NULL,
  `Price` int(20) DEFAULT NULL,
  `Amount` int(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_dt_pro` (`ProID`),
  CONSTRAINT `FK_dt_pro` FOREIGN KEY (`ProID`) REFERENCES `products` (`ProID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of orderdetails
-- ----------------------------
INSERT INTO `orderdetails` VALUES ('1', '1', '37', '200', '1000000', '200000000');
INSERT INTO `orderdetails` VALUES ('2', '1', '38', '101', '50000', '5050000');
INSERT INTO `orderdetails` VALUES ('3', '2', '11', '1', '3200000', '3200000');
INSERT INTO `orderdetails` VALUES ('4', '3', '36', '1', '200000', '200000');
INSERT INTO `orderdetails` VALUES ('5', '4', '36', '200', '200000', '40000000');
INSERT INTO `orderdetails` VALUES ('6', '4', '33', '200', '2100000', '420000000');
INSERT INTO `orderdetails` VALUES ('7', '4', '40', '100', '2000000', '200000000');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `OrderID` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `Email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `NgayDat` datetime DEFAULT NULL,
  `TongTien` int(20) DEFAULT NULL,
  `DiaChi` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TinhTrang` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`OrderID`),
  UNIQUE KEY `ID` (`OrderID`),
  KEY `FK_ord_users` (`Email`),
  CONSTRAINT `FK_ord_users` FOREIGN KEY (`Email`) REFERENCES `users` (`Email`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('3', 'mb@gmail.com', '2018-01-09 20:20:14', '200000', '123', null);
INSERT INTO `orders` VALUES ('4', 'mb@gmail.com', '2018-01-09 22:51:41', '660000000', 'nhà', null);

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `ProID` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `ProName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProDes` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProFullDes` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Nation` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ProPrice` int(10) DEFAULT NULL,
  `Quantity` int(3) DEFAULT NULL,
  `CatID` int(2) DEFAULT NULL,
  `View` int(5) DEFAULT NULL,
  `Sold` int(5) DEFAULT NULL,
  `NgayNhap` date DEFAULT NULL,
  `MaLoai` int(2) unsigned DEFAULT NULL,
  PRIMARY KEY (`ProID`),
  UNIQUE KEY `PK_ProID` (`ProID`),
  KEY `FK_pro_cat` (`CatID`),
  KEY `FK_pro_loai` (`MaLoai`),
  CONSTRAINT `FK_pro_cat` FOREIGN KEY (`CatID`) REFERENCES `categories` (`CatID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_pro_loai` FOREIGN KEY (`MaLoai`) REFERENCES `loairuou` (`MaLoai`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', 'Chivas 12', 'Chivas 12 năm, Nồng độ: 40%, Dung tích: 750ml', '<p>Là loại Rượu Chivas được cho ra lò sớm nhất trong chuỗi sản phẩm của Chivas<br>Mùi: hương thơm quyến rũ lan tỏa của thảo mộc hoang dã, cây thạch nam, mật hong và trái cây vườn.<br>Vị: hương vị nồng nàn, trọn vẹn của kem, mật ong, táo chín với hương vani, quả hạch nâu và kẹo bơ.<br>Nồng độ: 40%, Dung tích: 750ml<p>', 'Scotland', '550000', '0', '1', '106', '22', '2017-11-13', '5');
INSERT INTO `products` VALUES ('2', 'Chivas 18', 'Chivas 18 năm, Nồng độ: 40%, Dung tích: 750ml', '<p>Mùi : Mùi hương thơm nhiều lớp với hương trái cây khô , keo bơ và một ít vị khói\r\nVị : Nồng nàn êm điệu một cách khác biệt với vị Socola đắng thoảng Hương hoa thanh lịch . Dư vị kéo dài ấm áp và đáng nhớ<br>Nồng độ: 40%, Dung tích: 750ml<p>', 'Scotland', '1200000', '99', '1', '107', '27', '2017-11-13', '5');
INSERT INTO `products` VALUES ('3', 'Chivas 25', 'Chivas 25 năm, Nồng độ: 40%, Dung tích: 750ml', '<p>Rượu Chivas 25 _ Phiên bản mới có dung tích 750 ml , hộp da to , chỉ được sản xuất trong các phiên bản giới hạn đặc biệt , mỗi chai rượu có số Seri riêng . Chivas 25 năm là kết quả của sự pha trộn những loại rượu Whisky tốt nhất ở độ tuổi từ 25 năm hoặc nhiều hơn , hương vị của Chivas 25 năm là rất đặc biệt , hương thơm nhiều lớp của Trái cây khô , Kẹo bơ , Chocola đắng và một ít vị Khói . Với các ghi chú của Mai và Đào <p>', 'Scotland', '5200000', '100', '1', '1000', '24', '2017-11-13', '5');
INSERT INTO `products` VALUES ('4', 'Chivas ultis', 'Chivas Ultis, Nồng độ: 40%, Dung tích: 700ml', '<p>Rượu Chivas Regal Ultis, được hãng công bố ra thị trường vào tháng 10/2016, là dòng rượu đầu tiên của hãng phối trộn từ các dòng Malt Whisky với nhau (Single malt). Chivas Ultis được phối trộn từ 5 Single Malt vùng Speyside gồm: Tormore, Longmorn, Strathisla, Allt A’Bhainne và Braeval. \r\n\r\nChivas Regal Ultis với thiết kế mới khá sang trọng nhằm vinh danh 5 bậc thầy pha chế nhà Chivas từ năm 1909: Charles Howard, Charles Julian, Allan Baillie, Jimmy Lang và pha chế trưởng hiện tại Colin Scott.<p>', 'Scotland', '3850000', '100', '1', '101', '23', '2017-11-13', '5');
INSERT INTO `products` VALUES ('5', 'Chivas extra', 'Chivas Extra, Nồng độ: 40%, Dung tích: 700ml', '<p>Màu sắc Màu tối với ánh sáng ấm của hổ phách\r\nMũi Vị ngọt ngào của trái chín với một chút của lê chín và dưa ,kẹo bơ , socola sữa , và 1 chút của gừng\r\nNếm Vị ngọt của trái lê chín ,vanila , nền là mùi quế và hạnh nhân\r\nKết thúc Cảm nhận tròn đầy quanh miệng ,kéo dài, kết thúc với nhiều vị ngọt<p>', 'Scotland', '1500000', '100', '1', '26', '24', '2017-11-13', '5');
INSERT INTO `products` VALUES ('6', 'Hennessy Black', 'Hennessy Black, Nồng độ: 40%, Dung tích: 750ml', '<p>– Màu sắc : Vàng nhạt .\r\n– Hương mũi : Dễ chịu với hương tươi mới, hương cam .\r\n– Vị : Vị ngọt nhẹ của mật ong, nho , điểm xuyết vị cam quýt, chút hương hoa nhài .<p>', 'Pháp', '450000', '50', '2', '26', '25', '2017-11-13', '5');
INSERT INTO `products` VALUES ('7', 'Hennessy Very Special 1', 'Hennessy Very Special, Nồng độ: 40%, Dung tích: 700ml', '<p>Năm 1865, Maurice Hennessy – cha đẻ của dòng Richard Hennessy đã tạo ra một bảng đánh giá nhằm phân biệt chất lượng của từng loại rượu cognac. Rượu Hennessy đạt tiêu chuẩn 3 sao cognac có tên là  Hennessy VS (viết tắt của Very Special) cũng là dòng cognac phổ biến nhất trên thế giới.\r\n\r\nRượu Hennessy Very Special của Nhật với hình dáng mang đậm tính cổ điển từ những năm 1975 đến nay không thay đổi. Cũng như chất lượng vẫn giữ nguyên hương vị cho đến ngày nay. Vị rượu đậm đà, thơm hương cam quýt, gỗi sồi Pháp, táo, nho và hạnh nhân mang dư âm kéo dài hơn.\r\n\r\nXuất xứ: Cognac France – mụa tại nội địa Nhật Bản\r\n\r\nDung tích: 700ml\r\n\r\nĐộ cồn: 40%<p>', 'Pháp', '1100000', '50', '2', '27', '26', '2017-11-13', '2');
INSERT INTO `products` VALUES ('8', 'Hennessy XO', 'Hennessy XO, Nồng độ: 40%, Dung tích: 700ml', '<p>- Hennessy XO 70 Cl / 40 % là rượu Cognac XO đầu tiên , mang tính mạnh mẽ , hào phóng của Quý ông . Henessy X.O - Một nhãn hiệu của sự Xa hoa , Hình dáng chai được thiết kế năm 1947 mang lại cho Nó một vẻ đẹp riêng biệt so với các loại Rượu XO khác\r\n- Chất nước của Hennessy XO sẫm mầu và phức tạp hơn nhiều Hennessy VSOP . Vẻ bề ngoài của Hennessy XO vô cùng quyến rũ họa tiết trang trí hình Chùm nho chín vàng rơm trên thân chai\r\n- Hennessy XO có Hương vị nồng nàn phong phú của Trái cây sấy khô với các vị như : Mận , Quả Vả cảm giác như vụt qua bạn . Đến ngụm thứ 2 ta có thể thấy đặc hơn với Socola , Hạt tiêu đen , Hương Quế , Đinh hương cùng các loại gia vị khác . Vị cân bằng trong vòm miệng - Một Cognac Mạnh mẽ , Hài hòa , Thanh lịch , Nam tính với vẻ Hào nhoáng đến nao lòng<p>', 'Pháp', '3700000', '50', '2', '28', '27', '2017-11-13', '1');
INSERT INTO `products` VALUES ('9', 'Hennessy Very Special 2', 'Hennessy Very Special, Nồng độ: 40%, Dung tích: 700ml', '<p>Năm 1865, Maurice Hennessy – cha đẻ của dòng Richard Hennessy đã tạo ra một bảng đánh giá nhằm phân biệt chất lượng của từng loại rượu cognac. Rượu Hennessy đạt tiêu chuẩn 3 sao cognac có tên là  Hennessy VS (viết tắt của Very Special) cũng là dòng cognac phổ biến nhất trên thế giới.\r\n\r\nRượu Hennessy Very Special của Nhật với hình dáng mang đậm tính cổ điển từ những năm 1975 đến nay không thay đổi. Cũng như chất lượng vẫn giữ nguyên hương vị cho đến ngày nay. Vị rượu đậm đà, thơm hương cam quýt, gỗi sồi Pháp, táo, nho và hạnh nhân mang dư âm kéo dài hơn.\r\n\r\nXuất xứ: Cognac France – mụa tại nội địa Nhật Bản\r\n\r\nDung tích: 700ml\r\n\r\nĐộ cồn: 40%<p>', 'Pháp', '1500000', '50', '2', '50', '28', '2017-11-13', '2');
INSERT INTO `products` VALUES ('10', 'Hennessy Paradis', 'Hennessy Paradis, Nồng độ: 40%, Dung tích: 700ml', '<p>Dáng hình của vỏ chai đã gắn chặt với thế giới hơn một thế kỷ qua . Vị rượu đầm cùng với dáng vẻ mạnh mẽ , thanh lịch là điều quen thuộc với giới sành rượu\r\nHennessy Paradis Extra được pha chế từ 100 loại rượu cốt từ những khu vực trồng nho tốt nhất tại cognac , thông qua quá trình lựa chọn khắt khe những loại eau-de-vie hảo hạng nhất . Đây là một trong những loại sành điệu nhất của dòng rượu hennessy\r\nKhác với Hennessy XO , Hennessy Paradis mở ra khuynh hướng cho cách tận hưởng lạc thú tao nhã và lịch lãm<p>', 'Pháp', '17000000', '50', '2', '60', '29', '2017-11-13', '5');
INSERT INTO `products` VALUES ('11', 'Martell Cordon Bleu', 'Martell Cordon Bleu, Nồng độ: 40%, Dung tích: 700ml', '<p>Rượu Martell Cordon Bleu\r\nKể từ lúc được sáng tạo bởi Edouard Martell vào năm 1912, Cordon Bleuđã trở thành 1 huyền thoại thực sự. Hương vị của loại rượu này được đánh giá trội hơn hẳn các loại khác trong vùng Borderies, với việc giữ lại sự thiết kế của vỏ chai kể từ lúc được sáng tạo ra, Cordon Bleu mang dáng vẻ cổ điển giữa những loại Cognac danh tiếng khác. Là loại Cognac dành cho những người sành điệu.<p>', 'Pháp', '3200000', '79', '3', '72', '31', '2017-11-13', '5');
INSERT INTO `products` VALUES ('12', 'Martell V-SOP', 'Martell V-SOP, Nồng độ: 40%, Dung tích: 700ml', '<p>Hương vị : Thoang thoảng hương Nho , gỗ rừng và 1 chút hương vani . Tinh tế và cá tính\r\nVị : Phức hợp nhưng nhẹ nhàng thanh thoát trái cây khô<p>', 'Pháp', '900000', '80', '3', '81', '50', '2017-11-13', '2');
INSERT INTO `products` VALUES ('13', 'Martell XO', 'Martell XO, Nồng độ: 40%, Dung tích: 700ml', '<p>- Dung tích : 700 ml\r\n- Nồng độ : 40 %\r\n- Mùi : Tiêu đen , Ngò , Dâu tằm đỏ\r\n- Vị : Trái cây phong phú như Vả , Quả óc chó , Hạnh nhân , Sáp ong và Gỗ trầm hương\r\n- Xuất xứ : vùng Cognac , Pháp\r\n- Martell là nhà sản xuất hàng đầu về Cognac , luôn tạo ra dòng cognac tinh khiết , mềm mại , không sử dụng lại cặn rượu để chưng cất . Phần lớn Martell được sản xuất từ rượu nho vùng Borderies<p>', 'Pháp', '4400000', '80', '3', '90', '20', '2017-11-13', '1');
INSERT INTO `products` VALUES ('14', 'Martell Very Special', 'Martell Very Special, Nồng độ: 40%, Dung tích: 700ml', '<p>Martell VSOP Medaillon là loại rượu có thể dùng tiếp đãi từ những bữa tiệc lớn đến những cuộc họp mặt nhỏ, hoặc là một bữa tiệc thân mật hai người. Vị rượu nhẹ và dịu, rất thích hợp khi ướp lạnh hoặc pha. Nhãn trên chai là một huân chương màu vàng, có in chân dung của Vua Louis XIV.\r\n\r\nMartell là nhà sản xuất hàng đầu về cognac, luôn tạo ra dòng cognac tinh khiết, mềm mại, không sử dụng lại cặn rượu để chưng cất. Phần lớn Martell được sản xuất từ rượu nho vùng Borderies.<p>', 'Pháp', '1500000', '80', '3', '101', '1', '2017-11-13', '2');
INSERT INTO `products` VALUES ('15', 'Martell VSOP Medaillon', 'Martell V-SOP Medaillon, Nồng độ: 40%, Dung tích: 700ml', '<p>Rượu Martell VSOP Medaillon là loại rượu có thể dùng tiếp đãi từ những bữa tiệc lớn đến những cuộc họp mặt nhỏ, hoặc là một bữa tiệc thân mật hai người.\r\nLà loại rượu êm dịu, hương vị phức hợp tinh tế. Trên nhãn chai có chạm trổ huy chương vàng và chân dung của vua Louis XIV, vị vua trị vì triều đại Pháp khi Martell ra đời năm 1715. Rượu màu vàng hổ phách, thoang thoảng hương nho, gỗ rừng  và 1 chút hương vani tinh tế và cá tính. Rượu Martell có vị phức hợp nhưng nhẹ nhàng thanh thoát của trái cây khô.<p>', 'Pháp', '1150000', '80', '3', '101', '2', '2017-11-13', '2');
INSERT INTO `products` VALUES ('16', 'Remy Martin VSOP1', 'Remy Martin V-SOP, Nồng độ: 40%, Dung tích: 700ml', '<p>Mùi : Hương nồng nàn từ Trái cây mùa hè , đặc biệt là Mơ và Đào chín . Hương thơm nồng nàn tươi mát của Hoa dại , đặc biệt là Hoa Violet\r\nVị : Nồng nàn & mượt mà của sự pha trộn hoàn hảo giữa các loại Trái cây chín\r\nRượu Remy Martin VSOP thường được sử dụng làm đồ uống trong những Bữa tiệc sang trọng , các Cuộc vui sum họp bạn bè , làm quà biếu cho Người thân và Đối tác làm ăn<p>', 'Pháp', '1000000', '20', '4', '104', '3', '2017-11-13', '2');
INSERT INTO `products` VALUES ('17', 'Remy Martin V', 'Remy Martin V, Nồng độ: 40%, Dung tích: 750ml', '<p>Remy Martin V is a recent product by Rémy Martin. This product is an eau-de-vie de vin (direct translation water of life of wine because it\'s the distilled product of grape wine): Distilled Grape Spirit.\r\n\r\nThe grapes used for Remy V comes from vineyards, then pressed and distilled in the traditional manner using copper pot stills. The slow distillation leads to smaller batches,allowing flavors of fruit and the product\'s unique aromas. Ice-cold filtration helps to create a smooth & transparent drink.\r\n\r\nRemy V is consumed on the rocks it or mixed. The bottle was introduced to the US market in 2010, and is only sold in the US. It is not available for sale in the European Union. The price of Remy Martin V is around $45.<p>', 'Pháp', '1800000', '18', '4', '105', '6', '2017-11-13', '2');
INSERT INTO `products` VALUES ('18', 'Remy Martin VSOP2', 'Remy Martin V-SOP, Nồng độ: 40%, Dung tích: 700ml', '<p>Mùi : Hương nồng nàn từ Trái cây mùa hè , đặc biệt là Mơ và Đào chín . Hương thơm nồng nàn tươi mát của Hoa dại , đặc biệt là Hoa Violet\r\nVị : Nồng nàn & mượt mà của sự pha trộn hoàn hảo giữa các loại Trái cây chín\r\nRượu Remy Martin VSOP thường được sử dụng làm đồ uống trong những Bữa tiệc sang trọng , các Cuộc vui sum họp bạn bè , làm quà biếu cho Người thân và Đối tác làm ăn<p>', 'Pháp', '1200000', '20', '4', '104', '5', '2017-11-13', '2');
INSERT INTO `products` VALUES ('19', 'Remy Martin VSOP3', 'Remy Martin V-SOP, Nồng độ: 40%, Dung tích: 700ml', '<p>Mùi : Hương nồng nàn từ Trái cây mùa hè , đặc biệt là Mơ và Đào chín . Hương thơm nồng nàn tươi mát của Hoa dại , đặc biệt là Hoa Violet\r\nVị : Nồng nàn & mượt mà của sự pha trộn hoàn hảo giữa các loại Trái cây chín\r\nRượu Remy Martin VSOP thường được sử dụng làm đồ uống trong những Bữa tiệc sang trọng , các Cuộc vui sum họp bạn bè , làm quà biếu cho Người thân và Đối tác làm ăn<p>', 'Pháp', '1500000', '20', '4', '105', '6', '2017-11-13', '2');
INSERT INTO `products` VALUES ('20', 'Remy Martin XO', 'Remy Martin XO, Nồng độ: 40%, Dung tích: 700ml', '<p>Dung tích : 700 ml\r\nNồng độ : 40 %\r\nMàu sắc : Vàng hổ phách đậm\r\nNguồn gốc xuất xứ : CH Pháp\r\nMùi : Mùi của các loại Hoa Trắng đặc biệt là Hoa Nhài . Mùi Quả Mận ngọt , Quả Sung chín , Cam , Quế Nghiền , Hạt Phỉ\r\nVị : Kết cấu độc đáo . Đáng nhớ hương vị lưu lại lâu trong vòm họng .  Sự phong phú của hương vị , phức cảm và nồng nàn , hòa cùng hương vị ngọt ngào và sự cân bằng hoàn hảo<p>', 'Pháp', '2700000', '20', '4', '106', '7', '2017-11-13', '1');
INSERT INTO `products` VALUES ('21', 'Vodka Hà Nội', 'Vodka Hà Nội, Nồng độ: 39.5%, Dung tích: 700ml', '<p>- Dung tích : 300ml , 500ml , 700 ml , 750ml\r\n- Nồng độ : 29,5 % , 33 % , 39,5 %\r\n- Màu rượu : Trong suốt\r\n- Quy cách : 24 Chai / Két , 12 Chai / Két\r\n- Xuất xứ : Công ty CP Cồn & Rượu Hà Nội - Halico\r\n- Mùi : Có mùi vị đặc trưng của rượu làm từ gạo\r\n- Vị : Có vị ngọt êm dịu tự nhiên của ngũ cốc<p>', 'Việt Nam', '85000', '46', '5', '14', '12', '2017-11-13', '5');
INSERT INTO `products` VALUES ('22', 'Vodka Cánh Đồng Nga', 'Vodka Cánh Đồng Nga, Nồng độ: 30%, Dung tích: 500ml', '<p>Được sản xuất và đóng chai tại Nga, Vodka Cá sấu Vàng giữ nguyên hương vị vodka Nga truyền thống – trong suốt, tinh khiết như nước sông băng, mùi thơm êm dịu đặc trưng và hương vị cay nồng ngây ngất. Chính vì vậy, Vodka Cá sấu Vàng sớm được người tiêu dùng Việt Nam yêu thích, tạo nên nền tảng cơ bản thương hiệu Vodka Cá sấu tại Việt Nam.\r\nVodka Cá sấu Vàng được đóng chai 500ml, nút bi dập nổi. Quy cách 20 chai/thùng. <p>', 'Nga', '55000', '49', '5', '14', '10', '2017-11-13', '5');
INSERT INTO `products` VALUES ('23', 'Vodka Diamond', 'Vodka Diamond, Nồng độ: 40%, Dung tích: 750ml', '<p>Rượu Vodka nguyên là thứ rượu có nguồn gốc từ một số nước Đông Âu nhất là Nga , Ba Lan và Litva . Nó cũng có truyền thống lâu đời ở Bắc Âu<p>', 'Việt Nam', '530000', '50', '5', '13', '10', '2017-11-13', '5');
INSERT INTO `products` VALUES ('24', 'Vodka Men\'s Club', 'Vodka Men\'s Club, Nồng độ: 40%, Dung tích: 700ml', '<p>Nguyên liệu cồn hảo hạng \"Lux\" chưng cất từ các hạt lúa mạch được chọn lựa kỹ càng, cùng nguồn nước ngầm tinh khiết sử lý qua nhiều tầng tháp lọc, thẩm thấu qua hệ thống phim lọc bạc, các lớp ion thạch anh để loại bỏ tối đa các độc tố tạo nên dòng Vodka  đặc trưng với vị êm, thanh khiết, không gây đau đầu. <p>', 'Việt Nam', '200000', '50', '5', '14', '50', '2017-11-13', '5');
INSERT INTO `products` VALUES ('25', 'Vodka Absolute', 'Vodka Absolute, Nồng độ: 40%, Dung tích: 750ml', '<p>- Dung tích : 750 ml\r\n- Nồng độ : 40 %\r\n- Mầu sắc : Trong suốt tinh khiết\r\n- Quy cách : 12 Chai / Thùng\r\n- Nguồn gốc xuất xứ : Rượu nhập khẩu Thụy Điển\r\n- Mùi vị : Mùi thơm của lúa mạch , hương vị thanh mát đâm đà và có đặc tính nhẹ của trái cây . Vị êm mượt tinh khiết và đâm đà với phong cách cực kỳ dễ chịu<p>', 'Thụy Điển', '320000', '50', '5', '17', '100', '2017-11-13', '5');
INSERT INTO `products` VALUES ('26', 'Rosso Puglia 1', 'Rosso Puglia, Nồng độ: 12.5%, Dung tích: 500ml', '<p>Khi Primitivo là giống nho có xuất xứ từ áo được kết hợp với Merlot sinh ra từ Pháp sẽ tạo ra một dòng vang lạ lẫm và độc đáo hơn bao giờ hết. Vang bịch Ý Codici Tarantino Primitivo Merlot chính là đứa con của hai vị trên nhưng chúng được lớn lên và trưởng thành ở đất nước Ý.<p>', 'Ý', '500000', '10', '6', '16', '2', '2017-11-13', '3');
INSERT INTO `products` VALUES ('27', 'Rosso Puglia 2', 'Rosso Puglia, Nồng độ: 12.5%, Dung tích: 500ml', '<p>Khi Primitivo là giống nho có xuất xứ từ áo được kết hợp với Merlot sinh ra từ Pháp sẽ tạo ra một dòng vang lạ lẫm và độc đáo hơn bao giờ hết. Vang bịch Ý Codici Tarantino Primitivo Merlot chính là đứa con của hai vị trên nhưng chúng được lớn lên và trưởng thành ở đất nước Ý.<p>', 'Ý', '500000', '10', '6', '17', '3', '2017-11-13', '3');
INSERT INTO `products` VALUES ('28', 'Rosso Puglia 3', 'Rosso Puglia, Nồng độ: 12.5%, Dung tích: 500ml', '<p>Khi Primitivo là giống nho có xuất xứ từ áo được kết hợp với Merlot sinh ra từ Pháp sẽ tạo ra một dòng vang lạ lẫm và độc đáo hơn bao giờ hết. Vang bịch Ý Codici Tarantino Primitivo Merlot chính là đứa con của hai vị trên nhưng chúng được lớn lên và trưởng thành ở đất nước Ý.<p>', 'Ý', '500000', '10', '6', '18', '4', '2017-11-13', '3');
INSERT INTO `products` VALUES ('29', 'Rosso Puglia 4', 'Rosso Puglia, Nồng độ: 12.5%, Dung tích: 500ml', '<p>Khi Primitivo là giống nho có xuất xứ từ áo được kết hợp với Merlot sinh ra từ Pháp sẽ tạo ra một dòng vang lạ lẫm và độc đáo hơn bao giờ hết. Vang bịch Ý Codici Tarantino Primitivo Merlot chính là đứa con của hai vị trên nhưng chúng được lớn lên và trưởng thành ở đất nước Ý.<p>', 'Ý', '500000', '10', '6', '21', '4', '2017-11-13', '3');
INSERT INTO `products` VALUES ('30', 'Rosso Puglia 5', 'Rosso Puglia, Nồng độ: 12.5%, Dung tích: 500ml', '<p>Khi Primitivo là giống nho có xuất xứ từ áo được kết hợp với Merlot sinh ra từ Pháp sẽ tạo ra một dòng vang lạ lẫm và độc đáo hơn bao giờ hết. Vang bịch Ý Codici Tarantino Primitivo Merlot chính là đứa con của hai vị trên nhưng chúng được lớn lên và trưởng thành ở đất nước Ý.<p>', 'Ý', '500000', '10', '6', '21', '5', '2017-11-14', '3');
INSERT INTO `products` VALUES ('31', 'Vang Botalcura', 'Botalcura, Xuất xứ: Chile, Nồng độ: 13.5%, Dung tích: 700ml', '<p>If you look for a trustworthy thesis writing service and want to benefit from a higher grade, your editors, proofreaders, and instructors are here to lend you a hand. Some students afraid of hiring professional writers due to ethical issues. <p>', 'Ý', '800000', '30', '7', '3', '8', '2017-11-14', '3');
INSERT INTO `products` VALUES ('32', 'Vang Moscato (women)', 'Moscato (women), Xuất xứ : Ý, Nồng độ: 5%, Dung tích: 700ml', '<p>Kết hợp hoàn hảo với các món có vị cay nhờ nồng độ cồn thấp và tính ngọt cao. Bạn sẽ thấy bất ngờ khi kết hợp Moscato với các món có gừng, quế, thảo quả hay là nhiều hạt tiêu. Điều này sẽ giúp chị em không phải đau đầu lo lắng đồ ăn Việt không phù hợp với rượu vang,\r\nBạn hoàn toàn có thể lựa chọn 1 trong hai sản phẩm tùy thuộc theo sở thích và túi tiền của mình.<p>', 'Ý', '450000', '40', '7', '8', '6', '2017-11-14', '3');
INSERT INTO `products` VALUES ('33', 'Rượu rắn hổ mang', 'Nồng độ : 35%, Dung tích: 5000ml', '<p>Rượu rắn l&agrave; một loại rượu thuốc. Rắn được ng&acirc;m trong rượu gạo nồng độ cao. Th&ocirc;ng thường rắn được chọn ng&acirc;m l&agrave; rắn độc. Rượu ng&acirc;m nguy&ecirc;n con thường gặp rượu rắn ng&acirc;m theo số lẻ một con (bộ ba hay tam x&agrave;: thường l&agrave; một con rắn hổ mang, một con cạp nong, một con rắn r&aacute;o), bộ 5 hay ngũ x&agrave; (th&ecirc;m v&agrave;o bộ ba hai con rắn kh&aacute;c l&agrave; một con cạp nia, một con hổ tr&acirc;u hoặc sọc dưa) hoặc c&oacute; thể nhiều hơn năm loại rắn</p>\r\n<p>&nbsp;</p>', 'Việt Nam', '2100000', '-150', '7', '6', '209', '2017-11-14', '4');
INSERT INTO `products` VALUES ('34', 'Vang Passion', 'Vang Passion, Botalcura, Xuất xứ: Chile, Nồng độ: 13.5%, Dung tích: 750ml ', '<p>Hương vị nhẹ nhàng, dễ uống, màu đỏ sậm rất đặc trưng của các giống nho từ Chile , đây là phân khúc rượu rẻ tiền phù hợp với mọi yêu cầu của các tầng lớp trong xã hội , nói cách khác đây là một sản phẩm thay thế rượu vang mặt trời (Vang lưới chi lê)<p>', 'Chile', '100000', '30', '7', '5', '1', '2017-11-14', '3');
INSERT INTO `products` VALUES ('35', 'Vang Chile Ochagavia', 'Vang Chile Ochagavia, Xuất xứ: Chile, Nồng độ: 14%, Dung tích: 750ml', '<p>Vang Chile Ochagavia Reserva 1851 (red - white) được sản xuất và đóng chai từ một khu đất trồng nho duy nhất, và nho trồng ở những khu đất này.<p>', 'Chile', '320000', '40', '7', '9', '1', '2017-11-14', '3');
INSERT INTO `products` VALUES ('36', 'Bàu đá Bình Định', 'Bàu đá Bình Định, Nồng độ: 55% , Dung tích: 500ml', '<p>Theo lời truyền miệng của d&acirc;n gian, trước kia rượu B&agrave;u Đ&aacute; B&igrave;nh Định được chuy&ecirc;n d&ugrave;ng để tiến vua&hellip;v&agrave; từ đ&oacute; h&igrave;nh th&agrave;nh những n&eacute;t văn h&oacute;a trong việc thưởng thức rượu B&agrave;u Đ&aacute;. Khi uống rượu, đầu ti&ecirc;n rượu đựng trong lu s&agrave;nh hoặc canh phải được r&oacute;t v&agrave;o một c&aacute;i ve v&ograve;i, sau đ&oacute; người r&oacute;t cầm ve v&ograve;i giơ cao, ghi&ecirc;ng ve v&ograve;i để tạo th&agrave;nh 01 d&ograve;ng chảy từ tr&ecirc;n cao v&agrave;o ly uống rượu (thường gọi l&agrave; ly/ch&eacute;n hột m&iacute;t) sao cho ph&aacute;t ra tiếng k&ecirc;u r&oacute;c r&aacute;ch đồng thời rượu sủi bọt tăm nh&igrave;n rất hấp dẫn.</p>\r\n<p>&nbsp;</p>', 'Việt Nam', '200000', '-151', '7', '71', '208', '2017-11-15', '4');
INSERT INTO `products` VALUES ('37', 'Rượu mật ong', 'Rượu mật ong, Nồng độ: 40%, Dung tích: 1000ml', '<p>Nguyên cả tổ ong Ruồi, nguyên hình dáng, bao gồm từ mật + sáp + nhộng non + phấn hoa được bỏ vào bình! Đổ ngập rượu nếp 40 độ, sau 4 tháng là quý anh chị đã có 1 bình rượu ngâm nguyên cả tổ ong ruồi cực kì chất lượng, đẹp về hình dáng, ngon, ngọt thơm về vị.<p>', 'Việt Nam', '1000000', '-172', '7', '40', '203', '2017-11-15', '4');
INSERT INTO `products` VALUES ('38', 'Rượu nếp', 'Rượu nếp, Nồng độ: 45%, Dung tích: 1000ml', '<p>Gạo nếp dùng làm rượu nếp nguyên thủy là loại gạo nếp hạt ngắn, màu trắng đục. Thành phần tinh bột của gạo nếp chủ yếu là Amylopectin rất dễ hồ hóa và kết dính sau khi chín. <p>', 'Việt Nam', '50000', '-61', '7', '31', '102', '2017-11-15', '5');
INSERT INTO `products` VALUES ('39', 'Rượu nếp Gò Đen', 'Rượu nếp, Nồng độ: 45%, Dung tích: 1000ml', '<p>CÔNG DỤNG CỦA RƯỢU NẾP GÒ ĐEN  NGÂM TRÁI NHÀU KHÔ trái nhàu sắp chín Rượu nếp ngâm trái nhàu khô, là một loại rượu được mọi người ưa chuộng nhất hiện nay. Thứ nhất là rất rẻ, vì ở đâu cũng có thể tìm mua được. Có khi bà con con cho tặng không lấy tiền. Thứ hai, rượu ngâm nhàu phơi khô, cho vị thơm ngon, nên uống rất dễ. Rượu nếp ngâm trái nhàu không, hỗ trợ hệ tiêu hóa, giúp các hệ miễn dịch hoạt động tốt hơn. Rượu nếp Gò Đen...<p>', 'Việt Nam', '75000', '48', '7', '22', '3', '2017-11-15', '5');
INSERT INTO `products` VALUES ('40', 'Rượu sochu', 'Rượu sochu, Nồng độ: 20%, Dung tích: 750ml', '<p>Có hương vị khá giống với vodka của Mỹ, tuy nhiên soju có vị dịu hơn và ngọt hơn. So với vodka, soju cũng được coi là dễ uống hơn. Nguyên liệu để sản xuất ra soju là sẵn có nên giá thành của rượu soju so với các loại thức uống có cồn khác cũng rẻ hơn nhiều. Cũng chính vì thế soju đã trở thành một trong số những loại thức uống có cồn phổ biến nhất ở Hàn Quốc. <p>', 'Hàn Quốc', '2000000', '-1', '7', '515', '181', '2017-11-15', '5');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `Pass` varchar(100) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`Email`),
  UNIQUE KEY `PK_Email` (`Email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('admin', 'admin', '0192023a7bbd73250516f069df18b500');
INSERT INTO `users` VALUES ('Bao', 'mb@gmail.com', 'e10adc3949ba59abbe56e057f20f883e');
